#! /usr/bin/env racket
#lang racket
;(define dir (vector-ref (current-command-line-arguments) 0))
;(define working-directory (build-path (current-directory-for-user) dir))

(define bmp-dir-path  "bmps")
(define svg-dir-path  "svgs")
(make-directory* svg-dir-path)

(define bmp-files
  (filter (lambda (p)
	    (or (path-has-extension? p #".jpg")
                (path-has-extension? p #".png")
		(path-has-extension? p #".JPG")
		(path-has-extension? p #".jpeg")))
	  (directory-list bmp-dir-path)))
(define bmp-paths
  (map (lambda (p) (build-path bmp-dir-path p))
       bmp-files))
;; jpg-paths ; ok!
(define svg-paths
  (map (lambda (p) (build-path svg-dir-path p))
       (map (lambda (p) (path-replace-extension p ".svg"))
	    bmp-files)))
;; svg-paths ; ok!
(define bmp2svg
  (lambda (bmp-pth svg-pth)
    "convert .jpg or .png bitmap  file to .svg file using system calls to ImageMagick's `convert' and  then to `potrace'."
    (let* ((pnm-pth (path-replace-extension bmp-pth ".pnm"))
	   (pnm (path->string pnm-pth))
	   (svg (path->string svg-pth))
	   (bmp (path->string bmp-pth))
	   (convert (find-executable-path "convert"))
	   (potrace (find-executable-path "potrace"))
	   (rm (find-executable-path "rm")))
      (system* convert bmp pnm)
      (system* potrace "-b" "svg" "--tight" "-M" ".02" "--opaque" pnm "-o" svg)
      (system* rm pnm))))

(for-each bmp2svg bmp-paths svg-paths)
