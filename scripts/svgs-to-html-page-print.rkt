#! /usr/bin/env racket
#lang racket
;; ./imgbase-responses-txt-to-page-2coloption-hintsend.rkt C4-L5-more-in-on-is-are-my-her-after-break/ sentences.txt 29mm 2
;; ./imgbase-responses-txt-to-page-2col.rkt S2-C2/ sentences.txt 25mm 2
;; command-line 0 directory 1 text file 2 image height 3 column number
;; call with directory that contains sentences.txt svgs/
;; and with optional image-height and column arguments
;; (require simple-qr) ;; replace with system qrencode for Kanji etc
;; (define dir (vector-ref (current-command-line-arguments) 0))
(define dir (current-directory-for-user)) ;; for testing in directory with materials
(define qr-dir (build-path dir "qr-pngs"))
(unless (directory-exists? qr-dir) (make-directory qr-dir))
(define qr-count-n ; number
  (make-parameter 1)) ; prep starts with one
(define (qr-count-s) ; string
  (define now-count (qr-count-n))
  (qr-count-n (add1 now-count))
  (number->string now-count)) ; calls start with one
;; allow for different versions
(define (get-word-count line)
  (length (string-split line)))
(define (get-word-count-str line)
  (number->string (get-word-count line)))
(define (dialogue-line? line)
  (regexp-match #px"^\\w: +" line))
(define (dialogue-parts line)
  (match-define (list lne spkr sntnc)
		(regexp-match #px"^(\\w: +)(.*)" line))
  (list line spkr sntnc))

(define text-file
  (if (< 0 (vector-length  (current-command-line-arguments)))
      (vector-ref (current-command-line-arguments) 0)
      "sentences.txt"))
(define image-height ;; "22.5mm") ;; for testing in directory with materials
  (if (< 1 (vector-length (current-command-line-arguments)))
       (vector-ref (current-command-line-arguments) 1)
       "22mm")) ;; 22.5
(define image-margin-bottom ;; "1mm") ;; for testing quickly in dir with mats
   (if (< 2 (vector-length (current-command-line-arguments)))
       (vector-ref (current-command-line-arguments) 2)
       "1mm"))
(define repeat-string
  (lambda (num str)
    (let ((num (if (number? num) num (string->number num))))
    (define helper
      (lambda (n s keep)
	(cond ((= n 0) keep)
	      (#t (helper (- n 1) s (string-append s keep))))))
    (helper num str ""))))
(define cols ;; choice 2 columns, or one
  (if (< 4 (vector-length (current-command-line-arguments)))
      (repeat-string (vector-ref (current-command-line-arguments) 4) "1fr ")
      "2fr "))
      
;; (define working-directory (build-path (current-directory-for-user) dir))
(define text-file-path (build-path dir text-file))

(define get-img-and-sentences-list
  (lambda ()
    (let* ((input (open-input-file text-file-path))
	   (instring (port->string input)))
      (close-input-port input) 
      (string-split instring "\n\n"))))
(define imgbase-with-sentences (get-img-and-sentences-list))

(define imgbasestr-sentences-als ;; associative list
  (map (lambda (s) (string-split s "\n"))
       imgbase-with-sentences))
;; '("010" "This is my hand.")
;; '("300" "That is my pen.")

(require scribble/html) ; for output-xml
(require scribble/html/html) ; for div and class: etc...

(define worksheet-html
  (lambda (img-sen-als) ;img base-names and sentences a-list
    (let ((page-ttle "Worksheet")
	  (out-file (open-output-file
		     (path-replace-extension text-file-path ".html") #:exists 'replace )))
      (output-xml
       (html
	(head (title page-ttle)
	      (meta http-equiv: "Content-Type" content: "text/html;charset=utf-8")
	      (map style (list
			  (string-append "div.image-sentences-divs-container { margin-top: -1em; display: grid; grid-template-columns:" cols ";  } ")
			  "div.image-with-sentences {padding-bottom: 1mm;}"
			  (string-append "img { height: " image-height  "; margin: 2mm 10mm 1mm 2mm; float: left; }")
                          ;; ul.wordcount (display: block: float: left; )
                          "ol.word-count-ol { list-style-type: trad-chinese-informal; font-size: 8pt; }" ;; after float-right hint-qr and float-left svg
                          ;; margin-left: 25mm;
                          "li.word-count-li { display: list-item; margin-bottom: .5mm; font-size: 8pt; } "
                          "ol.sentence-w-WC { list-style-type: trad-chinese-informal; display: block; font-size: 8pt; font-family: Manjari;}"
                          "ol.sentence-w-WC > li {display: list-item; }"
                          "span.word-count-span { margin-right: 1em; } "
                          "span.word-count-span::before { content: ':' ; }"
                          "span.word-count-span::after    { content: ':'; }"
                          "span.answer-sentence { display: none; }"
                          "span.answer-sentenceshow {display: inline-block; } "
                          "span.show ( display: block; } "
			  "ul { margin: 1mm 1mm 2mm 4mm; padding: 0mm 1mm 2mm 4mm; list-style: none;  }" ; display: none;
			  ;; "li { display: none; font-size: 9pt; }" ; child selector `>' changed to &gt
			  "ul.show { display: block; margin: 1em 0 0 0; padding: 0 0 0 0; } "
			  "li.show {display: list-item;  margin-left: 1em; }"
                          ".hint-check {float: right;}"
                          ".sen-num {margin-top: 1mm; float: right; color: gray; } "
                          ;; later, put in middle of qr-code
                          ".qr-code { width: 50; height: 50; float: right; display: block;}"
                          (literal "div.hints > p { margin: -1mm 0 0 2mm; font-family: \"Manjari\"; font-size: 8pt; }") ;; was 1 mm, but firefox print went to two pages
			  "div.postamble { margin-top: -2mm; display: grid; grid-template-columns: 2fr 1fr 1fr;}"
			  (literal "div.postamble > p { margin-bottom: 0; font-size: 8pt; font-family: Manjari;}" ))))
	      (body
	       (div class: "image-sentences-divs-container"
		    (map image-sentence-div img-sen-als))
	       (div class: "hints"
		    (p
		     (get-unique-words img-sen-als))
		    ) ; end hints
	       (div class: "postamble"
		    (p class: "comments" "Comments: ")
		    (p class: "name"     "Name: ")
		    (p class: "number"   "Number: "))
	       )) ; end body, end html 
       out-file))))

(define img-sze "5")
(define (qr-sys-write txt fname)
  (define qrencoder (find-executable-path "qrencode"))
  ;;(define img-nme (path-add-extension fname ".png"))
  (system* qrencoder "-s" img-sze "-o" fname txt))

(define (sens->qr-png img sen-lst)
  (define txt (string-join sen-lst "\n"))
  (define expldd-pth (explode-path img))
  (define img-bse (last expldd-pth))
  (define lsn-dir ;;(if (> 2 (length expldd-pth))
    ;;(third (reverse expldd-pth))
    ;; need to fix path errors... !! TODO
    (last (explode-path dir)))
  (define png-bse (string-append (path->string lsn-dir) "-"
                                 (path->string img-bse) "-"
                                 (qr-count-s)))
                                 
  (define pth (build-path qr-dir (path-add-extension png-bse ".png")))
  (qr-sys-write txt pth) ;; write from parent dir
  pth) ;; return path from inner dir..
    
(define image-sentence-div
  (lambda (als-row) ; '("010" "This is my hand.")...
    (let* ((firstline (first als-row))
	   (splitline (string-split firstline #rx" +"))
	   (imgbase (first splitline))
	   (liclass (if (< 1 (length splitline))
			(second splitline)
			""))
           (sentences (cdr als-row)))
      (define (dialogue-line->li line)
        (match-define (list lne spkr sntnc)
          (dialogue-parts line))
        (define wc-str (get-word-count-str sntnc))
        (li class: (string-append "dialogue-sentence answer-li " liclass)
            (span class: "word-count-span" wc-str)
            (span class: "speaker-letter" spkr)
            (span class: (string-append "answer-sentence" liclass)
                  sntnc)))
      
      (define (plain-line->li line)
        (define wc-str (get-word-count-str line))
        (li class: liclass
            (span class: "word-count-span" wc-str)
            (span class: (string-append "answer-sentence" liclass)
                                        line)))
      
      (define (answer->li line)
        (if (dialogue-line? line)
            (dialogue-line->li line)
            (plain-line->li line)))
      
      (div class: "image-with-sentences"
           (div class: "hint-check" 
                (div class: "sen-num" (number->string (length sentences)))
                (img class: "qr-code"
                     src: (sens->qr-png imgbase (cdr als-row))))
	   (img src: ;; (build-path 'up
                                 (path-add-extension imgbase
                                                     ".svg"))
           ;;)
           (ol class: "sentence-w-WC"
               (map (lambda (s) (answer->li s)) sentences))
           
           ))))

;; for hints div section
(define get-unique-words 
  (lambda (als)
    (regexp-replace* #rx".?--+.?" 
     (string-join 
      (sort 
       (remove-duplicates
	(flatten
	 (map (lambda (sentences)
		(map (lambda (sntnc)
		       (string-split (marks-off sntnc) #rx" +"))
		     sentences))
	      (map cdr als))))
       string<?)
      " ")
      "")))
     

(define marks (regexp "[?.,!\"]"))
(define marks-off
  (lambda (str)
    (regexp-replace* marks str "" )))

(worksheet-html imgbasestr-sentences-als)
