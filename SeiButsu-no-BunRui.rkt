#lang slideshow
;; #lang slideshow/widescreen
(require rsvg racket/draw slideshow/text slideshow/play syntax/parse/define)
(require pict-abbrevs) ;; for save-pict and sharing pngs of come slides
;; save pict with last-slide->png

(include "slideshow-setup.rkt")
(current-main-font "IPA Pゴシック") ;; P明朝 とコンパクト、短い
;; start with big default text-size 56, work in fit-text later
(include "common-procedures.rkt")
(include "animation-settings.rkt")
(include "animation-reuse.rkt")
(define quiz? #t)
(define answers? #f)
(define org-table
  "| _*~*_ | 5界説 | _*~*_ | 3ドメイン説 | 
|    界    | 者 |             特徴             |  ドメイン  |
|   動物   |   消費   |       多細胞、ヒトを含む、有性生殖       |   _*~*_    |
|   植物   |   生産   |  多細胞、葉緑体、有性生殖   |  _*~*_  |
|    菌    |   分解   | カビ、キノコなど胞子を出す者 | 真核生物 |
| 原生生物 | いろいろ |            例外集            |   _*~*_    |
|  モネラ  | いろいろ |          非真核生物          |  真正細菌  |
|  _*~*_   |  _*~*_   |           >(非バクテリア新分類)>            |   古細菌   |")
;; _*~*_             |   古細菌   |")
(define org-tbl-rows (string-split org-table "\n")) ;; org-tbl-rows ; ok!
(define (org-row->strs rw)
  (string-split rw #px"(\\s+)?\\|(\\s+)?"))
(define org-tbl-strs (map org-row->strs org-tbl-rows))
(name-picts-n-list (list r1-1 r1-2 r1-3 r1-4)
		   (map bt (first org-tbl-strs))
		   r1) ;; (apply hbl-append 10 r1) ; ok!
(name-picts-n-list (list r2-1 r2-2 r2-3 r2-4)
		  (map bt (second org-tbl-strs))
		  r2)
(name-picts-n-list (list r3-1 r3-2 r3-3 r3-4)
		  (map t (third org-tbl-strs))
		  r3)
(name-picts-n-list (list r4-1 r4-2 r4-3 r4-4)
		  (map t (fourth org-tbl-strs))
		  r4)
(name-picts-n-list (list r5-1 r5-2 r5-3 r5-4)
		  (map t (fifth org-tbl-strs))
		  r5)
(name-picts-n-list (list r6-1 r6-2 r6-3 r6-4)
		  (map t (sixth org-tbl-strs))
		  r6)
(name-picts-n-list (list r7-1 r7-2 r7-3 r7-4)
		  (map t (seventh org-tbl-strs))
		  r7)
(name-picts-n-list (list r8-1 r8-2 r8-3 r8-4)
		  (map t (eighth org-tbl-strs))
		  r8)
(define rows (list r1 r2 r3 r4 r5 r6 r7 r8))

(define c1 (map first rows))
(define c1-slot (make-slot-for-answer-list c1))
(name-picts-n-list (list r1-1-s r2-1-s r3-1-s r4-1-s r5-1-s r6-1-s r7-1-s r8-1-s)
		   (map launder (make-list 8 c1-slot))
		   c1-slots) ;; (apply vc-append  c1-slots) ; ok!

(define col-1 (make-pct-slt-col c1-slots c1)) ; ok!
;; (slide (hc-append col-1 col-1)) ;; see whole frame in slide, not DrR

(define c2 (map second rows))
(define c2-slot (make-slot-for-answer-list c2))
(name-picts-n-list (list r1-2-s r2-2-s r3-2-s r4-2-s r5-2-s r6-2-s r7-2-s r8-2-s)
		   (map launder (make-list 8 c2-slot))
		   c2-slots) ;; (apply vc-append  c1-slots) ; ok!
(define-col-w-slots col-2 c2-slots c2) ;; (slide (hc-append 5 col-1 col-2)) ; ok!
;; need syntax to include view?? etc?
;; ; or just pin-over? 

(define c3 (map third rows))
(define c3-slot (make-slot-for-answer-list c3))
(name-picts-n-list (list r1-3-s r2-3-s r3-3-s r4-3-s r5-3-s r6-3-s r7-3-s r8-3-s)
		   (map launder (make-list 8 c3-slot))
		   c3-slots) ;; (apply vc-append  c1-slots) ; ok!
(define-col-w-slots col-3 c3-slots c3)
;; (slide (hc-append 2 col-1 col-2 col-3)) ;; ok! but might need smaller font

(define c4 (map fourth rows))
(define c4-slot (make-slot-for-answer-list c4))
(name-picts-n-list (list r1-4-s r2-4-s r3-4-s r4-4-s r5-4-s r6-4-s r7-4-s r8-4-s)
		   (map launder (make-list 8 c4-slot))
		   c4-slots) ;; (apply vc-append  c1-slots) ; ok!
(define-col-w-slots col-4 c4-slots c4)
;; (slide (hc-append 2 col-1 col-2 col-3 col-4)) ;; ok but domains need arrows.
;;  domains column has more rows, layout is off, try with table and pin-over

(define c1-blank (filled-rectangle (pict-width c1-slot)(pict-height c1-slot)
				    #:color "white" #:draw-border? #f))
(define c2-blank (filled-rectangle (pict-width c2-slot)(pict-height c2-slot)
				    #:color "white" #:draw-border? #f))
(define K5D3table (table 4 (flatten rows)
			 cc-superimpose cc-superimpose
			 (list 25 25 35) ;; column separation
			 20)) ;; row separation

(require ppict/align)
(define (blanker base hide)
  (define-values (x y) (cc-find base hide))
  (define blank (filled-rectangle (pict-width hide)(pict-height hide)
				  #:color "white" #:draw-border? #f))
  (pin-over/align base x y 'c 'c blank))

;; (define archaea-hint (t "非バクテリアの新分類"))
(define K5D3-blanked-1 ;; (cc-find K5D3table r1-1) ;; 64.0 18.095
  (let* ([p (pin-over/align K5D3table 64.0 18.095 'c 'c  c1-blank)]
	 [p (blanker p r1-3)]
	 [p (blanker p r3-4)]
	 [p (blanker p r4-4)]
	 [p (blanker p r6-4)]
	 [p (blanker p r8-1)]
	 [p (blanker p r8-2)]
	 ;; [p (blanker p r8-3)] ;; changed text to include hint
	 ;; add Archaea hint arrow-->
	 ) ;; (cc-find K5D3table r8-2)
    p)) ;; (slide K5D3-blanked-1) ;; ok!
;; (last-slide->png "imgs/SeiButsu-BunRui-study-table.png") ; ok!
(define (framer base elicit slot)
  (define-values (x y)(cc-find base elicit))
  (define boxer (filled-rectangle (pict-width slot) (pict-height slot) ;; gainsboro, 
				   #:color "ivory" #:draw-border? #t #:border-width 2))
  (pin-over/align base x y 'c 'c boxer))
;; find lighter color 

(define K5D3-blanked-Qs
  (let* ([p (framer K5D3-blanked-1 r3-1 c1-slot)]
	 [p (framer p r3-2 c2-slot)]
	 [p (framer p r4-1 c1-slot)]
	 [p (framer p r4-2 c2-slot)]
	 [p (framer p r5-1 c1-slot)]
	 [p (framer p r5-2 c2-slot)]
	 [p (framer p r6-1 c1-slot)]
	 [p (framer p r7-1 c1-slot)]
	 ;; domains
	 [p (framer p r5-4 c4-slot)]
	 [p (framer p r7-4 c4-slot)]
	 [p (framer p r8-4 c4-slot)]
	 [p (pin-arrow-line 10 p r8-3 rc-find r8-4 lc-find #:color "blue" #:line-width 2)]
	 )
    p))
;; (slide K5D3-blanked-Qs) ; and in DrR -> ;; K5D3-blanked-Qs

(define K5D3-blanks-arrows
  (let* ([p (pin-arrow-line 10 K5D3-blanked-Qs r3-1 rt-find r5-4 lt-find #:color "red" #:line-width 3 #:style 'dot #:start-angle (/ pi 11) #:end-angle (- (/ pi 11)))]
	 [p (pin-arrow-line 10 p r4-1 rc-find r5-4 lc-find #:color "red" #:line-width 3 #:style 'dot #:start-angle (/ pi 11) #:end-angle (- (/ pi 11)))]
	 [p (pin-arrow-line 10 p r5-1 rc-find r5-4 lc-find #:color "red" #:line-width 3 #:style 'dot #:start-angle (- (/ pi 11)) #:end-angle (/ pi 11))]
	 [p (pin-arrow-line 10 p r6-1 rb-find r5-4 lb-find #:color "red" #:line-width 3 #:style 'dot #:start-angle (- (/ pi 11)) #:end-angle (/ pi 11))]
	 ;; end Eukarya domain combination
	 ;; start splitting Monera into Bacteria and Archaea
	 [p (pin-arrow-line 10 p r7-1 rc-find r7-4 lt-find #:color "red" #:line-width 2)]
	 [p (pin-arrow-line 10 p r7-1 rb-find r8-4 lb-find #:color "red" #:line-width 2)]
	 )
    p))  ;; (slide K5D3-blanks-arrows) ; ok!
;; TODO!! later read in org-table and list of cells r2-1 r5-4 etc rROWNUM-CELLNUM
;; ; and dyanmically generate answer choice paragraph... 

(define answer-strings ;; put this pattern into syntax-pars
  (list "動物" "植物" "菌" "原生生物" "モネラ" "消費" "光合成" "生産" "分解" "真核生物" "真正細菌" "古細菌"))
(name-picts-n-list (list a1 a2 a3 a4 a5 a6 a7 a8 a9 a10 a11 a12)
		   (map t answer-strings)
		   answer-picts)
(define answers-Kai (cons (t "鉱物") (list a1 a2 a3 a4 a5)))
(define answers-Sha (cons (t "内部") (list a6 a7 a8 a9)))
(define answers-Dmn (cons (t "エオサイト") (list a10 a11 a12)))
;; (slide (para answer-picts)) ;; ok!
(define answer-slot (make-slot-for-answer-list answer-picts))
(name-picts-n-list (list s1 s2 s3 s4 s5 s6 s7 s8 s9 s10 s11 s12)
		   (map launder (make-list 12 answer-slot))
		   slot-picts)
;;  (frame (hc-append s1 s2)) ; ok!!

;; need a function to add answer-choices ..
(define table-blanked-w-answer-choices
  (vc-append 40  K5D3-blanked-Qs
	     (para (add-between (shuffle answer-picts) (t "、 ")))))
;; (slide table-blanked-w-answer-choices) ;; ok!
;; table-blanked-w-answer-choices ;; ok!

(define c-sep 20)
(define (intro-choices txt pct (sep 10) (punct ":"))
  (hbl-append sep (t (string-append txt punct)) pct))
(define choices-Kai (apply hbl-append c-sep (shuffle answers-Kai)))
(define choices-Sha (apply hbl-append c-sep (shuffle answers-Sha)))
(define choices-Dmn (apply hbl-append c-sep (shuffle answers-Dmn)))
(define choices-pct (vl-append 25
		     (intro-choices "界" choices-Kai)
		     (intro-choices "者" choices-Sha)
		     (intro-choices "ドメィン" choices-Dmn)))
(define K4D3-blanks-quiz
  (vc-append 50  K5D3-blanks-arrows
	     (with-size 62
			;;(para #:align 'center #:width client-w (shuffle answer-picts))
			choices-pct
			)))
K4D3-blanks-quiz ;; ok, better with Japanese "P" compact font "IPA Pゴシック"
; or "IPA P明朝"

(slide K4D3-blanks-quiz)
;; (last-slide->png "SeiButsu-no-BunRui-blanks-quiz-hint-arrows-slide-6.png") ;; Ok from DrRacket, but cli complains after $ slideshow SeiButu..... 
