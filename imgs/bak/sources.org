#+TITLE: Image Notes
#+STARTUP: showall
#+OPTIONS: toc:nil num:nil timestamp:nil
#+HTML_HEAD: <style> @page { size: 210mm 297mm; margin: 4mm; margin-bottom: 6mm; } </style>
#+HTML_HEAD: <style> h1.title { display: none; }  </style>
#+HTML_HEAD: <style> h1, h2, h3, h4 { font-weight: 500; font-size: 1.2em; }  </style>
#+HTML_HEAD: <style> div#postamble { display: none; }  </style>


** Cell structure

*** Typical Animal Cell halved view   
 - screenshot png
[[file:Eukarya-cell-structure.jpg]]

 - original svg
[[file:Biological_cell.svg]]

*** Japanese labels
   - 核小体
   - 細胞核
   - リボソーム (5の一部として点で示す)
   - 小胞
   - 粗面小胞体
   - ゴルジ体 (またはゴルジ装置)
   - 細胞骨格 (微小管, アクチンフィラメント, 中間径フィラメント)
   - 滑面小胞体
   - ミトコンドリア
   - 液胞
   - 細胞質基質 (細胞小器官を含む液体。これを元に細胞質は構成される)
   - リソソーム
   - 中心体
     
*** English labels
    English: Diagram of a typical animal cell. Organelles are labelled as follows:

   - Nucleolus
   - Nucleus
   - Ribosome
   - Vesicle
   - Rough endoplasmic reticulum
   - Golgi apparatus (or "Golgi body")
   - Cytoskeleton
   - Smooth endoplasmic reticulum
   - Mitochondrion
   - Vacuole
   - Cytosol
   - Lysosome
   - Centriole

*** Nucleus view
    
[[file:Nucleus_ER.svg]]
  
**** Japanese Labels
 1. 核膜
 2. リボソーム
 3. 核膜孔
 4. 核小体
 5. クロマチン
 6. 細胞核
 7. 小胞体
 8. 核質
 
**** English labels
     

*** Source: 
   - https://ja.wikipedia.org/wiki/%E7%B4%B0%E8%83%9E%E6%A0%B8
   - https://ja.wikipedia.org/wiki/%E7%9C%9F%E6%A0%B8%E7%94%9F%E7%89%A9
     - Screenshot 2023年  6月  5日 月曜日 16:10:29 JST
     - ja.wikipedia.org/wiki/%E3%83%95%E3%82%A1%E3%82%A4%E3%83%AB:Biological_cell.svg
     - upload.wikimedia.org/wikipedia/commons/1/1a/Biological_cell.svg
     - ja.wikipedia.org/wiki/%E3%83%95%E3%82%A1%E3%82%A4%E3%83%AB:Biological_cell.svg
       - svg - labels included, can edit text, great example
   - upload.wikimedia.org/wikipedia/commons/7/76/Nucleus_ER.svg
     
*** Resources
    - https://www.open.edu/openlearn/science-maths-technology/a-tour-the-cell/content-section-0/?printable=1
      
