#lang slideshow  ;; #lang slideshow/widescreen
;; rsvg --> can't use on proprietary systems where racket giver ffi errors
(require  racket/draw slideshow/text slideshow/play syntax/parse/define)
(require pict-abbrevs ppict/align)
(include "../../common-org-input-procedures.rkt")
(include "../../slideshow-setup.rkt")
(include "../../common-procedures.rkt")
(include "../../animation-settings.rkt")
(include "../../animation-reuse.rkt")

(define quiz? #t)
(define answers? #f)
;; (define page (bitmap "imgs/pg-screenshot.png"))
(define bmp-dir "imgs/pngs")
(define bmp-pths (directory-list #:build? #t bmp-dir))

bmp-pths

#;'(#<path:imgs/pngs/bak> #<path:imgs/pngs/bk-010.png> #<path:imgs/pngs/bk-020.png> #<path:imgs/pngs/bk-030.png> #<path:imgs/pngs/bk-040.png> #<path:imgs/pngs/ws-050.png>)

#;(name-picts-n-list (list bk-010 bk-020 bk-030 bk-040 ws-050)
		   (map bitmap bmp-pths) bk-komas) ;; book pics
;; bk-komas  bk-040 ; ok!
;; (list bk-040 ws-050) ; ok, width and height don't match but ok for now

;; use bk-komas name for pict and text combo?
