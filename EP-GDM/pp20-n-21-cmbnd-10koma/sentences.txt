../p20-He-give-hat-to-her/svgs/010
This is a man.
This is a hat.
This is his hand.
The hat is in his hand.
This is a woman.

../p20-He-give-hat-to-her/svgs/020
He will give the hat to her.

../p20-He-give-hat-to-her/svgs/030
He is giving the hat to her.

../p20-He-give-hat-to-her/svgs/040
He gave the hat to her.

../p20-He-give-hat-to-her/svgs/050
The hat was in his hand.
The hat is in her hands. 

../p21-She-put-hat-on-table/svgs/010
This is a woman.
This is a hat.
These are her hands.
The hat is in her hands.
This is  a table.

../p21-She-put-hat-on-table/svgs/020
She will put the hat on the table.

../p21-She-put-hat-on-table/svgs/030
She is putting the hat on the table.

../p21-She-put-hat-on-table/svgs/040
She put that hat on the table.

../p21-She-put-hat-on-table/svgs/100
The hat was in his hand.
The hat was in her hands.
The hat is on the table. 
