#lang racket

(require syntax/parse/define)

;; https://docs.racket-lang.org/syntax/Defining_Simple_Macros.html

(define-syntax-parse-rule (fn2 x y rhs)
    #:declare x id
    #:declare y id
    #:declare rhs expr
    (lambda (x y) rhs))
((fn2 a b (+ a b)) 3 4)

(define-syntax-parse-rule (fn2-t1 x:id y:id rhs:expr)
  (lambda (x y) rhs))
((fn2-t1 a b (- a b)) 4 3)

#;(define-syntax macro-id
  (syntax-parser parse-option ... clause ...))

 (define-syntax-parser fn3
    [(fn3 x:id rhs:expr)
     #'(lambda (x) rhs)]
    [(fn3 x:id y:id rhs:expr)
     #'(lambda (x y) rhs)])
((fn3 x x) 4)