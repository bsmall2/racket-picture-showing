#lang slideshow

(require pict-abbrevs)
;; Circular diagram for Local AgroEcology in Vandana Shiva's
;; ; Monocultures of the Mind 第一章 精神のモノカルチャー p.49
(define canvas (filled-rectangle client-w client-h #:color "white"))
				 

;; replace with syntax-parser from common procedures later
(match-define (list dirt-p water-p animal-p plant-p food-p)
	      (map t (list "土壌" "水" "動物" "植物" "食料")))
(define participants (list dirt-p water-p animal-p plant-p food-p))
(define parti-sep 100)
(define internal-participants
  (apply hc-append parti-sep participants))
;; labels feed compost fertilizer-1 fertilizer-2 humidity nutritients.
;; ; see Portuguese version if can't find English ... 
;; (slide internal-participants)
internal-participants

(define a-sze 20);; arrow-size
(define plant-food (pin-line internal-participants
			     food-p lc-find
			     plant-p rc-find
			     ))
			     
(define to-animals (pin-arrow-line a-sze plant-food
				   plant-p lb-find
				   animal-p cb-find
				   #:start-angle (- (/ pi 2))
				   #:end-angle (/ pi 2)
				   #:start-pull 3/4
				   #:end-pull 3/4
				   #:label (colorize (it "飼料") "gray")
				   #:x-adjust-label -25
				   #:y-adjust-label 65
				   ))
;; (slide to-animals)
;; (panorama to-animals)
(cc-superimpose canvas to-animals)
(define animals-to (pin-arrow-line a-sze to-animals
				   animal-p ct-find
				   plant-p lt-find
				   #:start-angle (/ pi 2)
				   #:end-angle (- (/ pi 2))
				   #:start-pull 3/4 ; default 1/4
				   #:end-pull 3/4 ; larger val longer
				   #:label (colorize (it "肥料") "gray")
				   #:x-adjust-label 20
				   #:y-adjust-label -35
				   ));; (cc-superimpose canvas animals-to); ok!
;; (slide (cc-superimpose canvas animals-to)) ; ok!
(define to-water (pin-arrow-line a-sze animals-to
				 plant-p cb-find
				 water-p cb-find
				 #:start-angle (- (/ pi 2))
				 #:end-angle (/ pi 2)
				 #:start-pull 3/4
				 #:end-pull 3/4
				 #:label (colorize (it "腐植質") "gray")
				 #:x-adjust-label -40
				 #:y-adjust-label 150))
(cc-superimpose canvas to-water)
(slide (cc-superimpose canvas to-water))

						  


  
