#lang racket
;; https://www.greghendershott.com/fear-of-macros/all.html#%28part._format-id%29

(require (for-syntax racket/syntax))
(require syntax/parse/define)

(define-syntax-parser hyphen-define/ok3
  [(_ a b (args ...) body ...+)
       #:with name (format-id #'a "~a-~a" #'a #'b)
         #'(define (name args ...)
             body ...)])

(hyphen-define/ok3 show true () #t)
(show-true) 
