#lang racket

(require (for-syntax racket/file  ))
(require syntax/parse/define)

;; just combing aoc-day-14 and greg-Our-struct to name
;; rows and cells read in from org-table? ? l

(define in-file "lucky-spirit-walkers.txt")
(define-syntax-parser text-to-lists-defs
  #:literals (f-str)
  ([(_ f-str)
   #:with str-lines (file->lines f-str)
   #'str-lines]))

(text-to-lists-defs in-file)
   
