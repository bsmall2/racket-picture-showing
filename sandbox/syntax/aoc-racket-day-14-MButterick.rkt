#lang racket
;; 2023年  7月  6日 木曜日 12:17:27 JST
;; ; https://docs.racket-lang.org/aoc-racket/Day_14.html

;; TODO, re-write with define- syntax-parser,
;; ;; redo story with 7 Lucky Spirits.. Play with text-> org-table
;; ;;  views of speeds, motion-time, resting-time..

;; ;; ; use as model to get org-table -> pict-table, with named picts
(require (for-syntax racket/file))

(define-syntax (convert-input-to-reindeer-functions stx)
  (syntax-case stx ()
    [(_)
     (let* ([input-strings (file->lines "reindeer-day14-input.txt")]
            [reindeer-strings
             (map (λ (str) (format "(reindeer ~a)" (string-downcase str))) input-strings)]
            [reindeer-datums
             (map (compose1 read open-input-string) reindeer-strings)])
       (datum->syntax stx `(begin ,@reindeer-datums)))]))
 
(define-syntax (reindeer stx)
  (syntax-case stx (can fly seconds but then must rest for)
    [(_ deer-name can fly speed km/s for fly-secs seconds,but then must rest for rest-secs seconds.)
     #'(define (deer-name total-secs)
         (calc-distance total-secs speed fly-secs rest-secs))]
    [else #'(void)]))
 
(convert-input-to-reindeer-functions)
 
(define (calc-distance total-secs speed fly-secs rest-secs)
  (let loop ([secs-remaining total-secs][distance 0])
    (if (<= secs-remaining 0)
        distance
        (let ([secs-in-flight (min secs-remaining fly-secs)])
          (loop (- secs-remaining fly-secs rest-secs)
                (+ (* secs-in-flight speed) distance))))))

(define (q1)
  (define seconds-to-travel 2503)
  (apply max (map (λ (deer-func) (deer-func seconds-to-travel))
                  (list dasher dancer prancer vixen comet
                        cupid donner blitzen rudolph))))
(q1)

(require sugar/list)
(define (q2)
  (define deer-funcs (list dasher dancer prancer vixen comet
                           cupid donner blitzen rudolph))
  (define winners
    (frequency-hash
     (flatten
      (for/list ([sec (in-range 1 (add1 2503))])
                (define deer-results
                  (map (λ (deer-func) (deer-func sec)) deer-funcs))
                (define max-result (apply max deer-results))
                (map (λ (deer-result deer-func)
                       (if (= deer-result max-result)
                           deer-func
                           empty))
                     deer-results deer-funcs)))))
 
  (apply max (hash-values winners)))
(q2)
"But what is the name of the reindeer/" 
