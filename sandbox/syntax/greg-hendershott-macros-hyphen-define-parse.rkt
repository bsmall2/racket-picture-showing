#lang slideshow
(require (for-syntax racket/syntax racket/string))
;; https://www.greghendershott.com/fear-of-macros/all.html
;; ; .. use a shorthand for nesting let: let* [a 0] ..

(require syntax/parse/define)

(define-syntax-parser dash-def-parse-let*
  [(_ (names ...) (args ...) body ...+)
   (let ([name-stxs (syntax->list #'(names ...))])
     (with-syntax ([name (datum->syntax (car name-stxs)
				(string->symbol
				 (string-join (for/list ([name-stx name-stxs])
						(symbol->string
						 (syntax-e name-stx)))
					      "-")))])
     #'(define (name args ...)
	 body ...)))])
(dash-def-parse-let* (add some nums) (a b c) (+ a b c))
(add-some-nums 4 5 6) ;; 15 ; ok!
(add-some-nums 10 30 40) ;; 80 ; ok! 

#;(define-syntax-parser dash-def-parse*
  [(_ (names ...) (args ...) body ...+)
   #:with (name-stxs ...)  (syntax->list #'(names ...))
   #:with name (string->symbol (string-join  (map (lambda (sym-stx) (symbol->string (syntax-e sym-stx)))
						 (syntax->list #'(name-stxs ...))) "-"))
   #'(define (name args ...) body ...)])
;; (dash-def-parse* (add san nums) (a b c) (+ a b c))
;; (add-san-nums)

				
  
       




             
