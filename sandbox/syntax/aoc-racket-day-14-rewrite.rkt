#lang racket

(require (for-syntax racket/file))
(require syntax/parse/define)

(define-for-syntax in-file "reindeer-day14-input.txt")

(define-syntax-parser convert-input-to-reindeer-functions
  [(stx) ;; a name in place-holder spot is useful for lexical-context lctxt
   (let* ([input-strings (file->lines in-file)]
          [reindeer-strings
           (map (λ (str) (format "(reindeer ~a)" (string-downcase str))) input-strings)]
          [reindeer-datums
           (map (compose1 read open-input-string) reindeer-strings)])
     (datum->syntax #'stx `(begin ,@reindeer-datums)))])
 
(define-syntax-parser reindeer
  #:datum-literals (can fly seconds but then must rest for)
  [(_ deer-name can fly speed km/s for fly-secs seconds,but then must rest for rest-secs seconds.)
     #'(define (deer-name total-secs)
         (calc-distance total-secs speed fly-secs rest-secs))]
    [else #'(void)])
 
(convert-input-to-reindeer-functions)
 
(define (calc-distance total-secs speed fly-secs rest-secs)
  (let loop ([secs-remaining total-secs][distance 0])
    (if (<= secs-remaining 0)
        distance
        (let ([secs-in-flight (min secs-remaining fly-secs)])
          (loop (- secs-remaining fly-secs rest-secs)
                (+ (* secs-in-flight speed) distance))))))

(define (q1)
  (define seconds-to-travel 2503)
  (apply max (map (λ (deer-func) (deer-func seconds-to-travel))
                  (list dasher dancer prancer vixen comet
                        cupid donner blitzen rudolph))))
(q1)

(require sugar/list)
;; (define (q2)
  (define deer-funcs (list dasher dancer prancer vixen comet
                           cupid donner blitzen rudolph))
  (define winners
    (frequency-hash
     (flatten
      (for/list ([sec (in-range 1 (add1 2503))])
                (define deer-results
                  (map (λ (deer-func) (deer-func sec)) deer-funcs))
                (define max-result (apply max deer-results))
                (map (λ (deer-result deer-func)
                       (if (= deer-result max-result)
                           deer-func
                           empty))
                     deer-results deer-funcs)))))


   (apply max (hash-values winners))
;; ) remove q2 def to play with winners
;; (q2)
;;"But what is the name of the reindeer/"

;; winners ; "The winner is: " ;; (first (sort (hash->list winners) #:key cdr >))
;; I wonder how to get the procedure name out for format?
(format "Winner: ~a" (first (sort (hash->list winners) #:key cdr >)))


         


