#lang curly-fn racket

;; https://docs.racket-lang.org/curly-fn/index.html

(#{list 1 % 3} 2)

(map #{- % 4} (range 9))

(#{apply list %&} 'a 'b 'c)

(define lst (list '(a 1) '(c 3) '(f 6) '(b 2) '(e 5) '(d 4)))

(${sort 

