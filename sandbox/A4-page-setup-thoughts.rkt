#lang racket

;; dimensions of page in millimeters
(define-values (w-p-mm h-p-mm) (values 297 210))
;; margin ratio
(define m-r 100) ;; 
;; (/ 297 210)
;; width margin and height margin
(define w-m (/ w-p-mm m-r))
(define h-m (/ h-p-mm m-r))

;; leave margins around page 
(define w-a-mm (- w-p-mm (* 2 w-m)))
(define h-a-mm (- h-p-mm (* 2 h-m)))

(/ w-a-mm h-a-mm)
