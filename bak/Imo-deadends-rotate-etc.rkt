#;(hbl-append 10 (t "科: ")
			 (apply hbl-append (add-between
					    (shuffle imo-ka-pcts)(t " , "))))
	     #;(htl-append 10  (t "特徴: ")
			 (para #:align 'center  (add-between
						 (shuffle imo-tkch-ps) (t ","))))
(define (rotate-up-4th p);; for map
  (rotate p (/ pi 4)))
(define (rotate-dn-9th p)
  (rotate p (- (/ pi 9))))
(match-define (list jaga-gnch-rtd satu-gnch-rtd naga-gnch-rtd)
	      (map rotate-up-4th (map launder
                                      (list jaga-gnch-p satu-gnch-p naga-gnch-p))))
(match-define (list casa-gnch-rtd sato-gnch-rtd)
	      (map rotate-dn-9th (map launder (list casa-gnch-p sato-gnch-p))))
